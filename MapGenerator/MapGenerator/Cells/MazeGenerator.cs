﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace MapGenerator
{
    class MazeGenerator
    {
        public Cell[,] Maze { get; private set; }
        public List<Cell> Solution { get; private set; }

        public void GenerateMaze() {
            Cell cell;
            Maze = new Cell[7, 14];
            for (int x = 0; x < Maze.GetLength(0); x++)
            {
                for (int y = 0; y < Maze.GetLength(1); y++)
                {
                    cell = new Cell();
                    cell.Location = new Vector2(x, y);
                    cell.Position = new Vector2(
                        x % 7 == 0 ? 30 + x : 30 + x * 30,
                        y % 14 == 0 ? 30 + y : 30 + y * 30);

                    cell.Border = x % 7 == 0 ? (byte)(cell.Border | 2) : cell.Border;
                    cell.Border = x % 7 == 6 ? (byte)(cell.Border | 4) : cell.Border;
                    cell.Border = y % 14 == 0 ? (byte)(cell.Border | 8) : cell.Border;
                    cell.Border = y % 14 == 13 ? (byte)(cell.Border | 1) : cell.Border;

                    cell.Wall = 0;

                    Maze[x, y] = cell;
                }
            }

            CreateMaze();
            Maze[0, 13].StartCell = true;
            Maze[6, 0].EndCell = true;
            //SolveMaze();
        }

        private void CreateMaze()
        {
            int randomCell;
            int totalCells = 7 * 14; // Hardcoded variables suck, but this is no framework, just a quick and dirty demo.
            int visitedCells = 0;
            Cell currentCell;
            List<Cell> neighbors = new List<Cell>();
            Stack<Cell> stepStack = new Stack<Cell>(); // To keep track of already visited cells, so we can make the backtracking easier.

            // Picking a random cell.
            currentCell = Maze[RandomNumber(7), RandomNumber(14)];
            visitedCells = 1;

            while (visitedCells < totalCells)
            {
                neighbors = FindNeighbors(currentCell);

                /* If there's one or more suitable neighbors, we go to one of them at random.
                 * This is also another ugly piece of code... but it works... might improve it
                 * later on... maybe... <yawn />
                 */
                if (neighbors.Count > 0)
                {
                    randomCell = RandomNumber(neighbors.Count);
                    if (neighbors[randomCell].Location.X > currentCell.Location.X)
                    {
                        currentCell.Wall = (byte)(currentCell.Wall | 4);
                        neighbors[randomCell].Wall = (byte)(neighbors[randomCell].Wall | 2);
                    }
                    else if (neighbors[randomCell].Location.X < currentCell.Location.X)
                    {
                        currentCell.Wall = (byte)(currentCell.Wall | 2);
                        neighbors[randomCell].Wall = (byte)(neighbors[randomCell].Wall | 4);
                    }
                    else if (neighbors[randomCell].Location.Y > currentCell.Location.Y)
                    {
                        currentCell.Wall = (byte)(currentCell.Wall | 1);
                        neighbors[randomCell].Wall = (byte)(neighbors[randomCell].Wall | 8);
                    }
                    else if (neighbors[randomCell].Location.Y < currentCell.Location.Y)
                    {
                        currentCell.Wall = (byte)(currentCell.Wall | 8);
                        neighbors[randomCell].Wall = (byte)(neighbors[randomCell].Wall | 1);
                    }

                    stepStack.Push(currentCell);
                    currentCell = neighbors[randomCell];
                    visitedCells++;
                }
                else
                {
                    currentCell = stepStack.Pop();
                }
            }
        }

        private void SolveMaze()
        {
            int randomCell;
            Cell currentCell;
            List<Cell> neighbors = new List<Cell>();
            Stack<Cell> stepStack = new Stack<Cell>(); // To keep track of already visited cells, so we can make the backtracking easier.

            Solution.Clear();
            // Picking a random cell.
            currentCell = Maze[0, 13];

            while (!currentCell.EndCell)
            {
                neighbors = SolutionNeighbors(currentCell);

                /* If there's one or more suitable neighbors, we go to one of them at random.
                 * This is also another ugly piece of code... but it works... might improve it
                 * later on... maybe... <yawn />
                 */
                if (neighbors.Count > 0)
                {
                    randomCell = RandomNumber(neighbors.Count);
                    stepStack.Push(currentCell);
                    currentCell = neighbors[randomCell];
                }
                else
                {
                    currentCell = stepStack.Pop();
                }
            }
            stepStack.TrimExcess();
            int stackCount = stepStack.Count;
            for (int i = 0; i < stackCount; i++)
            {
                Solution.Insert(0, stepStack.Pop());
            }
        }

        private List<Cell> SolutionNeighbors(Cell currentCell)
        {
            List<Cell> neighbors = new List<Cell>();
            switch (currentCell.Wall)
            {
                case 0:
                    break;
                case 1:
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    break;
                case 2:
                    if (!Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    break;
                case 3:
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    if (!Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    break;
                case 4:
                    if (!Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    break;
                case 5:
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    if (!Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    break;
                case 6:
                    if (!Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    if (!Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    break;
                case 7:
                    if (!Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    if (!Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    break;
                case 8:
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    break;
                case 9:
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    break;
                case 10:
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (!Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    break;
                case 11:
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (!Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    break;
                case 12:
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (!Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    break;
                case 13:
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (!Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    break;
                case 14:
                    if (!Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (!Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    if (!Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Visited)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    break;
                case 15:
                    break;
            }
            return neighbors;
        }

        /* This is probably the ugliest piece of code here, but anyway. It returns a
         * List<Step> with the possible cells we can actually get to.
         * 
         * The idea is that we look at the currentCell's neighbors. First we check if 
         * our currentCell happens to have a border; depending on that we can decide 
         * which ways to look. If... say, it's the bottom-right corner cell, then there
         * are no south and west cells; thus, we can only check the north and eastern cells.
         * 
         * Now, when we check the neighbor, we also check to see it has its walls intact. If
         * we happen to have already taken them down, then we don't go there. Codewise, if the
         * cell is a suitable option, it adds it to the List<Step> of possible neighbors.
         * 
         * Also, note that when checking north and south cells, remember that the y-axis goes
         * from down. Thus, the north cell is y - 1 and the south cell is y + 1. Funky!
         */
        private List<Cell> FindNeighbors(Cell currentCell)
        {
            List<Cell> neighbors = new List<Cell>();
            switch (currentCell.Border)
            {
                case 0:
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    if (Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    break;
                case 1:
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    if (Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    break;
                case 2:
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    break;
                case 3:
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    break;
                case 4:
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    break;
                case 5:
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y - 1]);
                    if (Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    break;
                case 6:
                case 7:
                case 8:
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    if (Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    if (Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    break;
                case 9:
                case 10:
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    if (Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X + 1, (int)currentCell.Location.Y]);
                    break;
                case 11:
                case 12:
                    if (Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X, (int)currentCell.Location.Y + 1]);
                    if (Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y].Wall == 0)
                        neighbors.Add(Maze[(int)currentCell.Location.X - 1, (int)currentCell.Location.Y]);
                    break;
                case 13:
                case 14:
                case 15:
                    break;
            }
            return neighbors;
        }

        /* I know, I know, these guys have no reason of being there. They should exist in
         * some kind of utility static class or something. Nevertheless, here they are.
         * Hello random number generators!
         */
        public int RandomNumber()
        {
            Random random = new Random();
            return random.Next();
        }

        public int RandomNumber(int max)
        {
            Random random = new Random();
            return random.Next(max);
        }

        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
    }
}
