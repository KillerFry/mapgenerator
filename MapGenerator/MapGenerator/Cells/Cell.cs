﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace MapGenerator
{
    class Cell
    {
        public Vector2 Location { get; set; }
        public Vector2 Position { get; set; }
        
        public bool StartCell { get; set; }
        public bool EndCell { get; set; }
        public bool Visited { get; set; }

        private Texture2D spriteUntouched;
        public Texture2D WallTexture { get; set; }

        /* Now, this two here are funky. Since I'm trying to save some CPU cycles
         * and as much RAM as possible, these members are bytes and will be manipulated
         * using binary operators... basically, just a simple OR --> '|' <--. Here's how
         * they work
         * 
         *      Border          Wall  
         *      0 0 0 0         0 0 0 0
         *      N E W S         N E W S
         *      
         * Sooo, what it means is. If a bit is set to 0 on border, it means there's no border
         * there; if you flip it to 1 on... say... the W position, it means the cell borders
         * with the screen on the west side of it.
         * 
         * For Wall, a bit value of 0 means the walls are standing up, and 1 means it's been
         * taken down and therefore an open side.
         * 
         * <Looks at the code and realizes a single byte could hold info on both the
         * border and wall, since a byte holds 8-bits... and thinks: "One day, I'll fix it." />
         */

        public Byte Wall { get; set; }
        public Byte Border { get; set; }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(WallTexture, Position, StartCell ? Color.Red : EndCell ? Color.Blue : Color.White);
        }
    }
}
