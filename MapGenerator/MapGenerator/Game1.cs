using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace MapGenerator
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        MazeGenerator maze;

        Texture2D[] wallTextures = new Texture2D[16];

        Generator caGenerator = new Generator();

        float waitTime = 1;

        List<Keys> prevPressedKeys = new List<Keys>();
        List<Keys> pressedKeys = new List<Keys>();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            graphics.PreferredBackBufferHeight = 900;
            graphics.PreferredBackBufferWidth = 1200;
            graphics.ApplyChanges();
            this.IsMouseVisible = true;

            maze = new MazeGenerator();
            maze.GenerateMaze();

            caGenerator.InitMap();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            /*for (int i = 0; i < wallTextures.GetLength(0); i++)
            {
                wallTextures[i] = Content.Load<Texture2D>("wall" + i.ToString());
            }

            for (int x = 0; x < maze.Maze.GetLength(0); x++)
            {
                for (int y = 0; y < maze.Maze.GetLength(1); y++)
                {
                    maze.Maze[x, y].WallTexture = wallTextures[maze.Maze[x, y].Wall];
                }
            }*/

            caGenerator.WallTexture = Content.Load<Texture2D>("Wall");
            caGenerator.FloorTexture = Content.Load<Texture2D>("Floor");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            prevPressedKeys = pressedKeys;
            pressedKeys = Keyboard.GetState().GetPressedKeys().ToList();

            if (!pressedKeys.Contains(Keys.F5) && prevPressedKeys.Contains(Keys.F5))
            {
                //caGenerator.InitMap();
                caGenerator.Generation();
            }

            if (!pressedKeys.Contains(Keys.F4) && prevPressedKeys.Contains(Keys.F4))
            {
                caGenerator.InitMap();
                //caGenerator.Generation();
            }
            // TODO: Add your update logic here

            /*if (waitTime > 0)
            {
                waitTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (waitTime < 0)
                {
                    waitTime = 5;
                    caGenerator.Generation();
                }
            }*/

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();
            for (int x = 0; x < maze.Maze.GetLength(0); x++)
            {
                for (int y = 0; y < maze.Maze.GetLength(1); y++)
                {
                    //maze.Maze[x, y].Draw(spriteBatch);
                }
            }
            caGenerator.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
