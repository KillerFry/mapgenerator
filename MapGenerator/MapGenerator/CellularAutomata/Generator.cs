﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Threading;

namespace MapGenerator
{
    class Generator
    {
        readonly int FLOOR = 0;
        readonly int WALL = 1;
        readonly int SIZE_X = 116;
        readonly int SIZE_Y = 86;
        readonly int r1Cutoff = 5;
        readonly int r2Cutoff = 4;
        int generationCount = 500;

        Random randomGenerator = new Random(DateTime.Now.Millisecond);

        Vector2 Location { get; set; }
        Vector2 Position { get; set; }

        public Texture2D WallTexture { get; set; }
        public Texture2D FloorTexture { get; set; }

        int[,] grid;
        int[,] grid2;

        public void InitMap()
        {
            grid = new int[SIZE_X, SIZE_Y];
            grid2 = new int[SIZE_X, SIZE_Y];

            int x, y;

            /*
             * Fills all inner cells (not the borders) as a Floor or a Wall at random.
             */
            for (x = 0; x < SIZE_X; x++)
            {
                for (y = 0; y < SIZE_Y; y++)
                {
                    if (x % SIZE_X == 0 || x % SIZE_X == SIZE_X - 1 || // We hit a left or right border cell
                        y % SIZE_Y == 0 || y % SIZE_Y == SIZE_Y - 1) // We hit a top or bottom border cell.
                    {
                        grid[x, y] = WALL;
                    }
                    else
                    {
                        grid[x, y] = RandPic();
                    }
                }
            }

            /*
             *  Fills the second grid with all walls.
             */
            for (x = 0; x < SIZE_X; x++)
            {
                for (y = 0; y < SIZE_Y; y++)
                {
                    grid2[x, y] = WALL;
                }
            }

            //generationCount = 3;
        }

        public void Generation()
        {
            if (generationCount == 0)
            {
                return;
            }

            int x, y, i, j;

            for (x = 1; x < SIZE_X - 1; x++)
            {
                for (y = 1; y < SIZE_Y - 1; y++)
                {
                    int adjCount_r1 = 0;
                    int adjCount_r2 = 0;

                    /*
                     * Here we check how many of a cell's neighbor's are walls. Eventually we will allow the cell
                     * to stay a wall if r1Cutoff is met.
                     */
                    for (i = -1; i <= 1; i++)
                    {
                        for (j = -1; j <= 1; j++)
                        {
                            if (grid[x + i, y + j] != FLOOR)
                            {
                                adjCount_r1++;
                            }
                        }
                    }

                    /*
                     * This is a little funky. The idea behind this is to check if a cell r2Cutoff spaces away is a wall.
                     * If there are r2Cutoff or more walls that are 2 spaces away, then we will turn it into a floor.
                     * Technically this allows us to have "columns" in the middle of cave rooms, instead of big open spaces.
                     */
                    for (i = x - 3; i <= x + 3; i++)
                    {
                        for (j = y - 3; j <= y + 3; j++)
                        {
                            if (Math.Abs(i - x) == 3 && Math.Abs(j - y) == 3)
                            {
                                continue;
                            }

                            if (i < 0 || j < 0 || i >= SIZE_X || j >= SIZE_Y)
                            {
                                continue;
                            }

                            if (grid[i, j] != FLOOR)
                            {
                                adjCount_r2++;
                            }
                        }
                    }

                    if (generationCount > 495)
                    {
                        if (adjCount_r1 >= r1Cutoff || adjCount_r2 <= r2Cutoff)
                        {
                            grid2[x, y] = WALL;
                        }
                        else
                        {
                            grid2[x, y] = FLOOR;
                        }
                    }
                    else
                    {
                        if (adjCount_r1 >= r1Cutoff)
                        {
                            grid2[x, y] = WALL;
                        }
                        else
                        {
                            grid2[x, y] = FLOOR;
                        }
                    }
                }
            }

            for (x = 1; x < SIZE_X - 1; x++)
            {
                for (y = 1; y < SIZE_Y - 1; y++)
                {
                    grid[x, y] = grid2[x, y];
                }
            }

            generationCount--;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < SIZE_X; x++)
            {
                for (int y = 0; y < SIZE_Y; y++)
                {
                    spriteBatch.Draw(grid[x, y] == WALL ? WallTexture : FloorTexture,
                        new Vector2(
                            x % SIZE_X == 0 ? 20 + x : 20 + x * 10,
                            y % SIZE_Y == 0 ? 20 + y : 20 + y * 10), Color.White);
                }
            }
        }

        private int RandPic()
        {
            if (randomGenerator.Next(0, 100) < 40)
            {
                return WALL;
            }
            else
            {
                return FLOOR;
            }
        }
    }
}
